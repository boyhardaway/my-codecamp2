

const db = require('../lib/db.js')

module.exports = {
    async myQuery() {
        let [rows, fields] = await db.query("SELECT * FROM user")
        console.log("The solution is: ", rows, fields)
        return rows
      }
}