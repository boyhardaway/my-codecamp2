const Koa = require("koa")
const Router = require("koa-router")
const serve = require("koa-static")
const render = require("koa-ejs")
const path = require("path")

const app = new Koa()
const router = new Router()

render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
})
 
const func = require('./models/query.js')

router.get("/", async ctx => { 
  let obj = {}
  obj.peoples = await func.myQuery()

  await ctx.render("homework11_3", obj)
})

app.use(serve(path.join(__dirname, "public")))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)
