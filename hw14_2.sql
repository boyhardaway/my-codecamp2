SELECT s.id, s.name, count(1),
    sum(c.price)
FROM
    courses c,
    enrolls e,
    students s
WHERE
    c.id = e.course_id AND s.id = e.student_id
GROUP BY
    s.id,
    s.name;
	
	
SELECT
    tb.id,
    tb.name,
    cc.name as course_name,
    tb.price
FROM
    (
    SELECT
        s.id,
        s.name,
        MAX(c.price) AS price
    FROM
        courses c,
        enrolls e,
        students s
    WHERE
        c.id = e.course_id AND s.id = e.student_id
    GROUP BY
        s.id,
        s.name
) AS tb,
courses cc,
enrolls ee
WHERE
    cc.id = ee.course_id AND tb.id = ee.student_id AND cc.price = tb.price
ORDER BY
    tb.id;



SELECT
    s.id,
    s.name,
    c.name as course_name,
    avg(c.price) AS price
FROM
    courses c,
    enrolls e,
    students s
WHERE
    c.id = e.course_id AND s.id = e.student_id
GROUP BY
    s.id,
    s.name;
	
	
	
