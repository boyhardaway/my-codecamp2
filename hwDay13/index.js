const Koa = require("koa");
const Router = require("koa-router");
const serve = require("koa-static");
const render = require("koa-ejs");
const path = require("path");

const app = new Koa();
const router = new Router();

render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
});

const mysql = require("mysql2/promise");
const pool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "",
  database: "online_course"
});

async function myQuery(sql) {
  let [rows, fields] = await pool.query(sql)
  return rows;
}

router.get("/", async ctx => {
  let obj = {}
  obj.instructors = await myQuery("SELECT i.name as instructors_name, c.name FROM instructors i left outer JOIN courses c on c.teach_by = i.id where c.name is null ")
 
  obj.courses = await myQuery("SELECT c.name, i.name as instructors_name FROM courses c LEFT outer JOIN instructors i on c.teach_by = i.id where i.name is null")

  
  await ctx.render("homework13_1", obj)
});

router.get("/hw14", async ctx => {
  let obj = {}
  let sql = 'select Sum(t.total_price) as total_price From (select c.id, c.name, c.price, count(1) as count_std, count(1) * c.price as total_price '
  sql = sql + ' From courses c, enrolls e where c.id = e.course_id group by c.id, c.name, c.price) t '
  obj.prices = await myQuery(sql)

  sql = 'select s.id, s.name, sum(c.price) as total_price '
  sql = sql + ' From courses c, enrolls e , students s where c.id = e.course_id and s.id = e.student_id group by s.id, s.name  '
  obj.students = await myQuery(sql)
  
  await ctx.render("homework14_1", obj)
});

app.use(serve(path.join(__dirname, "public")));
app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);
