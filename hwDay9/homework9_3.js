'use strict'
let percent_bar = 0
let photo1 = '<img id="photo_1" src="http://www.effigis.com/wp-content/uploads/2015/02/Iunctus_SPOT5_5m_8bit_RGB_DRA_torngat_mountains_national_park_8bits_1.jpg">'
let photo2 = '<img id="photo_2" src="http://www.effigis.com/wp-content/uploads/2015/02/Infoterra_Terrasar-X_1_75_m_Radar_2007DEC15_Toronto_EEC-RE_8bits_sub_r_12.jpg">'
let photo3 = '<img id="photo_3" src="http://www.effigis.com/wp-content/uploads/2015/02/DigitalGlobe_WorldView1_50cm_8bit_BW_DRA_Bangkok_Thailand_2009JAN06_8bits_sub_r_1.jpg">'


$('#myModal').modal('show')
$(function () {
    fetch('homework2_1.json').then(response => {
        return response.json()
    })
        .then(res => {
            genTable(res)
            update_progress_bar()
            $('#divphoto').append($(photo1))

            $("#photo_1").on("load", function () {
                update_progress_bar()
                $('#divphoto').append($(photo2))

                $("#photo_2").on("load", function () {
                    update_progress_bar()
                    $('#divphoto').append($(photo3))
                    
                    $("#photo_3").on("load", function () { 
                        //do
                    })
                })
            })
        })
        .catch(error => {
            console.error('Error:', error)
        })
})


function update_progress_bar() {
    percent_bar += 25
    let progressUp = "width:" + percent_bar + "%"
    console.log(progressUp)
    $('#progress_loading').attr("style", progressUp)
    if (percent_bar === 100) {
        setTimeout(hindModadl, 1000)
    }
}

function hindModadl() {
    $('#myModal').modal('hide')
}

// function loadingImage(htmlStr) {
//     return new Promise(function (resolve, reject) {
//         $('#divphoto').append($(htmlStr))
//     })
// }

// async function genHtml() {
//     try {

//         $('#divphoto').append($(photo1))
//         $('#divphoto').append($(photo2))
//         $('#divphoto').append($(photo3))
//         await loadingImage(photo1)
//         await loadingImage(photo2)
//         await loadingImage(photo3) 
//         update_progress_bar()
//     } catch (error) {
//         console.error(error)
//     }
// }
// genHtml()

function genTable(employees) {
    let htmlH = '<thead><tr>'
    for (let objSalaryHd in employees[0]) {
        htmlH += '<th>' + objSalaryHd + '</th>'
    }
    htmlH += '</tr></thead><tbody>'
    $('#myTable').append($(htmlH))

    let htmlD = ''
    for (let i = 0; i < employees.length; i++) {
        htmlD += '<tr>'
        for (let objSalaryDt in employees[i]) {
            htmlD += '<td>' + employees[i][objSalaryDt] + '</td>'
        }
        htmlD += '</tr>'
    }
    htmlD += '</tbody>'
    $('#myTable').append($(htmlD))
}

