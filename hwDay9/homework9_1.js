let fs = require('fs');
function myWriteFile(dataRobot) {
  return new Promise(function(resolve, reject) {
    fs.writeFile('robot.txt', dataRobot, 'utf8', function(err) {
      if (err)
        reject(err);
      else
        resolve();
    });
  });
}
function myReadFile(path) {
  return new Promise(function(resolve, reject) {
    fs.readFile(path, 'utf8', function(err, dataDemo1) {
      if (err)
        reject(err);
      else
        resolve(dataDemo1);
    });
  });
}


async function genFile() {
  try {
    var path = ['txtFile/head.txt','txtFile/body.txt','txtFile/leg.txt','txtFile/feet.txt'];
    let dataRobot = '';
    for (let i of path){
      dataRobot +=  await myReadFile(i) + '\n'; 
    }
    await myWriteFile(dataRobot); 
  } catch (error) {
    console.error(error);
  }
}
genFile();
