let fs = require('fs');

var p1 = new Promise(function (resolve, reject) {
  fs.readFile('txtFile/head.txt', 'utf8', function(err, dataDemo1) {
    if (err)
      reject(err);
    else
      resolve(dataDemo1);
  });
});
var p2 = new Promise(function (resolve, reject) {
  fs.readFile('txtFile/body.txt', 'utf8', function(err, dataDemo1) {
    if (err)
      reject(err);
    else
      resolve(dataDemo1);
  });
});
var p3 = new Promise(function (resolve, reject) {
  fs.readFile('txtFile/leg.txt', 'utf8', function(err, dataDemo1) {
    if (err)
      reject(err);
    else
      resolve(dataDemo1);
  });
});
var p4 = new Promise(function (resolve, reject) {
  fs.readFile('txtFile/feet.txt', 'utf8', function(err, dataDemo1) {
    if (err)
      reject(err);
    else
      resolve(dataDemo1);
  });
});
Promise.all([p1, p2, p3, p4])
.then(function(result){
  console.log('All completed!: ', result); // result = ['one','two','three']
  fs.writeFile('robot.txt', result[0] + '\n' + result[1] + '\n' + result[2] + '\n' + result[3], 'utf8', function(err) {
    if (err)
      reject(err);
    else
      resolve();
  });
})
.catch(function(error){
  console.error("There's an error", error); 
});
