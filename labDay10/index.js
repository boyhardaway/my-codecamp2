const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async ctx => {
    await ctx.render('index')
})

router.get('/about', async ctx => {
    await ctx.render('about')
})


// app.use(ctx => {
//     // console.log(ctx.path)
//     if (ctx.path === '/about') {
//         ctx.body = 'Not Found Page'
//     } else {

//         ctx.body = 'boy'
//     }
// })

// router.get('/', ctx => {
//     ctx.body = `<img src="images/tent1.jpg">`
// })

// router.get('/contact', ctx => {
//     ctx.body = 'this is contact page'
// })

// router.get('/about', ctx => {
//     ctx.body = 'this is about page'
// })

// console.log(path.join(__dirname, 'public'))
app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)