select DISTINCT c.id, c.name, i.name as instructor_name, c.price
from enrolls el, courses c, instructors i
where el.course_id = c.id
and c.teach_by = i.id;

select DISTINCT c.id, c.name, i.name as instructor_name, c.price
from enrolls el RIGHT OUTER JOIN courses c
on el.course_id = c.id,
instructors i
where el.student_id is null
and  c.teach_by = i.id;
order by c.id;


SELECT DISTINCT
    c.id,
    c.name as courses_name,
    i.name AS instructor_name,
    c.price
FROM
    courses c
LEFT OUTER JOIN enrolls el ON
    (el.course_id = c.id)
LEFT OUTER JOIN instructors i ON
    (c.teach_by = i.id)
WHERE
    el.student_id IS NULL
ORDER BY
    c.id;


SELECT DISTINCT
    c.id,
    c.name as courses_name,
    i.name AS instructor_name,
    c.price
FROM
    courses c
LEFT OUTER JOIN enrolls el ON
    (el.course_id = c.id)
INNER JOIN instructors i ON
    (c.teach_by = i.id)
WHERE
    el.student_id IS NULL
ORDER BY
    c.id;	