const Koa = require('koa')
const Router = require('koa-router') 

const app = new Koa()
const router = new Router()

router.get('/', async (ctx, next) => {
  ctx.myVariable = 'forbidden'; // assume variable
  console.log(1)
  await next()
})

async function myMiddleware (ctx, next) {
  console.log(2)
  if (ctx.myVariable == 'forbidden')
      ctx.body = "Access Denied"
  else
      await next()
} 

app.use(myMiddleware)
app.use(router.routes())
app.use(myMiddleware)
app.use(myMiddleware)// Not use
app.listen(3000)


// const Koa = require('koa')
// const app = new Koa()
// app.use(async (ctx, next) => {
//    console.log(ctx.path + ' : 1');
//    await next();
// })
// app.use(async (ctx, next) => {
//    console.log('2');
//    await next();
// })
// app.use(async (ctx, next) => {
//    console.log('3');
//    await next();
// })
// app.listen(3000)