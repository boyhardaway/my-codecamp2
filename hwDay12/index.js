const Koa = require("koa");
const Router = require("koa-router");
const serve = require("koa-static");
const render = require("koa-ejs");
const path = require("path");

const app = new Koa();
const router = new Router();

render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
});

const mysql = require("mysql2/promise");
const pool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "",
  database: "bookstore"
});

async function myQuery(sql) {
  let [rows, fields] = await pool.query(sql); 
  return rows;
}

router.get("/", async ctx => {cddxxyhyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyl
  let obj = {};
  obj.employees = await myQuery("select * from employee");
 
  obj.books = await myQuery("select * from book");

  
  await ctx.render("homework12_1", obj);
});

app.use(serve(path.join(__dirname, "public")));
app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);
