'use strict'
let percent_bar = 0




$('#myModal').modal('show')
// $(function () {
// $("#photo_1").one("load", function () {
// }).each(function () {
//     if (this.complete) {
//         // update_progress_bar();
//         setTimeout(update_progress_bar, 1200)
//     }
// });

// $("#photo_2").one("load", function () {
// }).each(function () {
//     if (this.complete) {
//         // update_progress_bar();
//         setTimeout(update_progress_bar, 1200)
//     }
// });

// $("#photo_3").one("load", function () {
// }).each(function () {
//     if (this.complete) {
//         // update_progress_bar();
//         setTimeout(update_progress_bar, 1200)
//     }
// });

$("#photo_1").on("load", function () {
    update_progress_bar();
}).each(function () {
    if (this.complete) {
        update_progress_bar();
    }
});

$("#photo_2").on("load", function () {
    update_progress_bar();
}).each(function () {
    if (this.complete) {
        update_progress_bar();
    }
});

$("#photo_3").on("load", function () {
    update_progress_bar();
}).each(function () {
    if (this.complete) {
        update_progress_bar();
    }
});

fetch('homework2_1.json').then(response => {
    return response.json()
})
    .then(res => {
        genTable(res)
        // update_progress_bar();
        setTimeout(update_progress_bar, 1200)
    })
    .catch(error => {
        console.error('Error:', error)
    })
// })


function genTable(employees) {
    let htmlH = '<thead><tr>'
    for (let objSalaryHd in employees[0]) {
        htmlH += '<th>' + objSalaryHd + '</th>'
    }
    htmlH += '</tr></thead><tbody>'
    $('#myTable').append($(htmlH))

    let htmlD = ''
    for (let i = 0; i < employees.length; i++) {
        htmlD += '<tr>'
        for (let objSalaryDt in employees[i]) {
            htmlD += '<td>' + employees[i][objSalaryDt] + '</td>'
        }
        htmlD += '</tr>'
    }
    htmlD += '</tbody>'
    $('#myTable').append($(htmlD))
}

function update_progress_bar() {
    percent_bar += 25
    let progressUp = "width:" + percent_bar + "%"
    console.log(progressUp)
    $('#progress_loading').attr("style", progressUp)
    if (percent_bar === 100) {
        setTimeout(hindModadl, 1000)
    }
}

function hindModadl() {
    $('#myModal').modal('hide')
}