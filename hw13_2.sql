INSERT INTO `students`(`name`) VALUES ('Jo');
INSERT INTO `students`(`name`) VALUES ('Torbo');
INSERT INTO `students`(`name`) VALUES ('Roronoa');
INSERT INTO `students`(`name`) VALUES ('Shanks');
INSERT INTO `students`(`name`) VALUES ('Ann');
INSERT INTO `students`(`name`) VALUES ('Ace');
INSERT INTO `students`(`name`) VALUES ('Luffy');
INSERT INTO `students`(`name`) VALUES ('Sabo');
INSERT INTO `students`(`name`) VALUES ('Nara');
INSERT INTO `students`(`name`) VALUES ('Kid');

INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (2,2);
INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (3,11);
INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (4,5);
INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (5,6);
INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (6,7);
INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (7,7);
INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (8,7);
INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (9,8);
INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (10,2);
INSERT INTO `enrolls`(`student_id`, `course_id`) VALUES (11,1);

select DISTINCT c.id, c.name
from enrolls el, courses c
where el.course_id = c.id;

select DISTINCT c.id, c.name, el.student_id
from enrolls el RIGHT OUTER JOIN courses c
on el.course_id = c.id
where el.student_id is null
order by c.id;