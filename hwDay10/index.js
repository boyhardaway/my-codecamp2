const Koa = require("koa");
const Router = require("koa-router");
const serve = require("koa-static");
const render = require("koa-ejs");
const path = require("path");

const app = new Koa();
const router = new Router();

render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
});

  
router.get("/", async ctx => {
  await ctx.render("aboutMe");
});

router.get("/skill", async ctx => {
  await ctx.render("skill");
});

router.get("/contactMe", async ctx => {
  await ctx.render("contactMe");
});

router.get("/portfolio", async ctx => {
  await ctx.render("portfolio");
}); 

app.use(serve(path.join(__dirname, "public")));

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);