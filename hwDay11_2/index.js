const Koa = require("koa");
const Router = require("koa-router");
const serve = require("koa-static");
const path = require("path");
const mysql = require("mysql2/promise");

const app = new Koa();
const router = new Router();

app.use(serve(path.join(__dirname, "public")));
const pool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "",
  database: "codecamp"
});

let obj = {};

router.get("/from_database", async (ctx, next) => {
  let [rows, fields] = await pool.query("SELECT * FROM user");
  obj.data = rows;
  await next();
  ctx.body = obj;
});

router.get("/from_file", async (ctx, next) => {
  let fs = require("fs");
  let dataJson = new Promise(function (resolve, reject) {
    fs.readFile("public/json/homework2_1.json", "utf8", function (err, dataRet) {
      if (err) reject(err);
      else resolve(dataRet);
    });
  });
  obj.data = JSON.parse(await dataJson);
  await next();
  ctx.body = obj;
});

async function changeJson(ctx, next) {
  let dateNow = new Date()
  let dateShow = `${dateNow.getFullYear()}-${dateNow.getMonth()}-${dateNow.getDate()} ${dateNow.getHours()}:${dateNow.getMinutes()}:${dateNow.getSeconds()}`
  obj.additionalData = {
    userId: 1,
    dateTime: dateShow
  };
  await next();
}

app.use(router.routes());
app.use(changeJson);
app.use(router.allowedMethods());
app.listen(3000);
