const Koa = require("koa");
const Router = require("koa-router");
const serve = require("koa-static");
const render = require("koa-ejs");
const path = require("path");

const app = new Koa();
const router = new Router();

render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
});

router.get("/", async ctx => {

  let fs = require('fs');
  let dataJson = new Promise(function (resolve, reject) {
    fs.readFile("public/json/homework2_1.json", 'utf8', function (err, dataRet) {
      if (err)
        reject(err);
      else
        resolve(dataRet);
    });
  });
  let obj = {}
  obj.peoples = JSON.parse(await dataJson)

  await ctx.render("peopleSalary", obj);
});

app.use(serve(path.join(__dirname, "public")));
app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);