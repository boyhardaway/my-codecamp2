const Koa = require("koa");
const Router = require("koa-router");
const serve = require("koa-static");
const render = require("koa-ejs");
const path = require("path");

const app = new Koa();
const router = new Router();

render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
});

const mysql = require("mysql2/promise");
const pool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "",
  database: "codecamp"
});

async function myQuery() {
  let [rows, fields] = await pool.query("SELECT * FROM user");
  console.log("The solution is: ", rows, fields);
  return rows;
}

router.get("/", async ctx => {
  let obj = {};
  obj.peoples = await myQuery();

  await ctx.render("homework11_1", obj);
});

app.use(serve(path.join(__dirname, "public")));
app.use(router.routes());
app.use(router.allowedMethods());
app.listen(3000);
