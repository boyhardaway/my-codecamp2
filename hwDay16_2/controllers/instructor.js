module.exports = function (instructorModel, pool) {
    return {
        async findAll(ctx, next) {
            const obj = await instructorModel.findAll(pool);
            
            await ctx.render('instructor', {
                instructors: obj
            }); 
            await next();
        },
        async findById(ctx, next) {
            const obj = await instructorModel.findById(pool, ctx.params.id);
            console.log(obj)
            await ctx.render('instructor', {
                instructors: obj
            });
            await next();
        }
    }
}