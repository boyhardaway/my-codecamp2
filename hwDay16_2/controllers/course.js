module.exports = function (courseModel, pool) {
    return {
        async findById(ctx, next) {
            const obj = await courseModel.findById(pool, ctx.params.id);
            console.log(obj)
            await ctx.render('course', {
                courses: obj
            });
            await next();
        },
        async findByPrice(ctx, next) {
            const obj = await courseModel.findByPrice(pool, ctx.params.price);
            console.log(obj)
            await ctx.render('course', {
                courses: obj
            });
            await next();
        }
    }
}