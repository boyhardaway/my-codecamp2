const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')

const app = new Koa()
const router = new Router()

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})
 

const db = require('./lib/db.js') 
const instructorModel = require('./models/instructor')
const tempController = require('./controllers/instructor')
const instructorController = tempController(instructorModel, db);

const courseModel = require('./models/course')
const temp2Controller = require('./controllers/course')
const courseController = temp2Controller(courseModel, db);
 

router.get('/instructor/find_all', instructorController.findAll)

router.get('/instructor/find_by_id/:id', instructorController.findById)

router.get('/course/find_by_id/:id', courseController.findById)

router.get('/course/find_by_Price/:price', courseController.findByPrice)

// router.get('/find_user_by_id/:id', async (ctx, next) => {
//   ctx.body = await userModel.fi(pool, ctx.params.id);
//   await next();
// });

// router.get('/instructor/find_all', async (ctx, next) => {   
//   let [rows] =  await db.query("SELECT * FROM instructors ")
//   ctx.body = rows
//   await next()
// })

// router.get('/instructor/find_by_id/:id', async (ctx, next) => {   
//   let [rows] =  await db.query("SELECT * FROM instructors where id = ? ", ctx.params.id)
//   ctx.body = rows
//   await next()
// })

// router.get('/course/find_by_price/:price', async (ctx, next) => {   
//   let [rows] =  await db.query("SELECT * FROM courses where price = ? ", ctx.params.price)
//   ctx.body = rows
//   await next()
// })
 
app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)
