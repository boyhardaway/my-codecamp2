module.exports = {
    createEntity(row) {
       // console.log(row)
       if (!row.id)
           return [];
        
       return  {
           id: row.id,
           name: row.name, 
           detail: row.detail, 
           price: row.price, 
           teach_by: row.teach_by, 
           created_at: row.created_at
       }
   },


   async findByPrice(pool, id) {
    const [rows] = await pool.query('SELECT * FROM courses WHERE price = ?', [price]);
    return rows.map(this.createEntity)
},
   async findById(pool, id) {
       const [rows] = await pool.query('SELECT * FROM courses WHERE id = ?', [id]);
       return rows.map(this.createEntity)
   }
   // ,
   // async findAll(pool){
   //     const [rows] =  await db.query("SELECT * FROM instructors where id = ? ", ctx.params.id)
   //     return rows.map(this.createEntity)
   // }
}