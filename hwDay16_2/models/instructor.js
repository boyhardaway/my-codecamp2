module.exports = {
     createEntity(row) {
        // console.log(row)
        if (!row.id)
            return [];
         
        return  {
            id: row.id,
            name: row.name, 
            created_at: row.created_at
        }
    },


    async findAll(pool){
        const [rows] =  await pool.query("SELECT * FROM instructors ")
        return rows.map(this.createEntity)
    },
    async findById(pool, id) {
        const [rows] = await pool.query('SELECT * FROM instructors WHERE id = ?', [id]);
        return rows.map(this.createEntity)
    }
    // ,
    // async findAll(pool){
    //     const [rows] =  await db.query("SELECT * FROM instructors where id = ? ", ctx.params.id)
    //     return rows.map(this.createEntity)
    // }
}