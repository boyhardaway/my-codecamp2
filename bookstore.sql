-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2018 at 05:50 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `isbn` varchar(13) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`isbn`, `title`, `price`, `create_at`) VALUES
('9783598215766', 'His Toy', 289, '2018-05-27 04:25:28'),
('9783598215919', 'The Mars Room', 199, '2018-05-27 04:25:14'),
('9783598215933', 'The Marquis and I', 250, '2018-05-27 04:24:46'),
('9783598215957', 'Blood on the Tongue', 352, '2018-05-27 04:24:46'),
('9783598215995', 'A Higher Loyalty', 157, '2018-05-27 04:25:14');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `firstname`, `lastname`, `age`, `create_at`, `address`) VALUES
(1, 'Noah', 'Armstrong', 34, '2018-05-27 03:21:13', 'BANGKOK'),
(2, 'Leslie', 'Ellis', 24, '2018-05-27 03:22:36', NULL),
(3, 'Sandra', 'Hoffman', 35, '2018-05-27 03:22:36', NULL),
(4, 'Cameron', 'Meyer', 19, '2018-05-27 03:22:36', NULL),
(5, 'Cameron', 'Moore', 27, '2018-05-27 03:22:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sale_inf`
--

CREATE TABLE `sale_inf` (
  `isbn` varchar(13) NOT NULL,
  `id` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_inf`
--

INSERT INTO `sale_inf` (`isbn`, `id`, `price`, `qty`, `created_at`) VALUES
('9783598215933', 1, 250, 4, '2018-05-27 04:40:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`isbn`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_inf`
--
ALTER TABLE `sale_inf`
  ADD PRIMARY KEY (`isbn`,`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
