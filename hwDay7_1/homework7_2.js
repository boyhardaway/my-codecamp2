'use strict'
// const fetch = require('node-fetch')
fetch('homework2_1.json').then(response => {
    return response.json();
})
    .then(peopleSalary => {
        let peopleLowSalary = peopleSalary
            .filter(checkSalary)
            .map(addSalary);
            console.log(peopleSalary);
        let sumSalary = peopleSalary.reduce((sum, num) =>   parseFloat(sum) + parseFloat(num.salary), 0);
 
        // let low = peopleSalary.map(each => {
        //     return each.salary < 100000 ? each.salary * 2 : each.salary
        // }).reduce((sum, each) => parseFloat(sum) + parseFloat(num.salary), 0);

       
        console.log('-----------------------========----------------------');
        console.log(sumSalary)
        genTable(peopleSalary, sumSalary)
        // genTable(sumSalary)
    })
    .catch(error => {
        console.error('Error:', error);
    });


function checkSalary(num) {
    return num.salary < 100000;
}

function addSalary(num) {
    num.salary = num.salary * 2
    return num;
}

function getTotal(total, num) {
    return total.salary * num.salary;
}

function genTable(employees, sumSalary) {
    let htmlH = '<thead><tr>'; 
    for (let objSalaryHd in employees[0]) { 
        htmlH += '<th>' + objSalaryHd + '</th>'
    }
    htmlH += '</tr></thead><tbody>'; 
    $('#myTable').append($(htmlH));

    let htmlD = '';
    for (let i = 0; i < employees.length; i++) {
        htmlD += '<tr>';
        for (let objSalaryDt in employees[i]) {
            htmlD += '<td>' + employees[i][objSalaryDt] + '</td>';
        }
        htmlD += '</tr>'; 
    }
    htmlD += '<tr><td></td><td></td><td></td><td></td><td>' + sumSalary + '</td></tr>';
    htmlD += '</tbody>';
    $('#myTable').append($(htmlD));
}