'use strict'
fetch('homework1-4.json').then(response => {
        return response.json();
    })
    .then(res => {
        console.log(res);

        let peoples = res.filter(each => {
                return each.gender === 'male' && each.friends.length >= 2;
            })
            .map((res) => {
                // debugger;
                let peopleObj = {};
                peopleObj.name = res.name;
                peopleObj.gender = res.gender;
                peopleObj.company = res.company;
                peopleObj.email = res.email;
                peopleObj.friends = res.friends;
                let money = precisionRound(Number(res.balance.replace(/[^0-9\.-]+/g, "")) / 10, 2);
                peopleObj.balance = '$' + money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return peopleObj
            })
        // .map(({name}) => name )

        console.log(peoples);
        genTable(peoples);
    })
    .catch(error => {
        console.error('Error:', error);
    });

function precisionRound(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
}

function genTable(peopleObj) {
    let htmlH = '<thead><tr>';
    for (let objSalaryHd in peopleObj[0]) {
        htmlH += '<th>' + objSalaryHd + '</th>'
    }
    htmlH += '</tr></thead><tbody>';
    $('#myTable').append($(htmlH));

    let htmlD = '';
    for (let i = 0; i < peopleObj.length; i++) {
        htmlD += '<tr>';
        for (let objSalaryDt in peopleObj[i]) {

            if (objSalaryDt === 'friends') {
                htmlD += '<td><ol>';
                for (let objFriends in peopleObj[i][objSalaryDt]) {
                    htmlD += '<li>' + peopleObj[i][objSalaryDt][objFriends].name + '</li>';
                }
                htmlD += '</td></ol>'
            } else {
                htmlD += '<td>' + peopleObj[i][objSalaryDt] + '</td>';
            }

        }
        htmlD += '</tr>';

    }
    htmlD += '</tbody>';
    $('#myTable').append($(htmlD));
}