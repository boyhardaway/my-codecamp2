let arr = [1,2,3,4,5,6,7,8,9,10];

function modTwo(num){
    return num % 2 === 0
}

function multiply(num){
    return num * 1000
}

let res = arr
.filter(modTwo)
.map(multiply);

console.log(res);