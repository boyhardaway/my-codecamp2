class Employee {
    constructor(firstname, lastname, salary) {
      let _salary = salary; // simulate private variable
  
      this.firstname = firstname; // public property
      this.lastname = lastname; // public property
      this.getSalary = function() { // simulate public method
        return _salary;
      };
      this.setSalary = function(salary) { // simulate public method
        return _salary = salary;
      };
    }
    hello() { // simulate public method
      console.log("Hello "+this.firstname+"!");
    }
  }

    let dang = new Employee('Dang','Red', 10000);
    console.log(dang.firstname); // Dang
    console.log(dang.lastname); // Red
    dang.hello(); // Hello Dang!
    console.log(dang.getSalary()) // 10000
    dang.setSalary(8000); //
    console.log(dang.getSalary()) // 8000 