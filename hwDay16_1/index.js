const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')

const app = new Koa()
const router = new Router()

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false,
  debug: true
})
 

const db = require('./lib/db.js')

 

router.get('/', async ctx => {  

})

router.get('/instructor/find_all', async (ctx, next) => {   
  let [rows] =  await db.query("SELECT * FROM instructors ")
  ctx.body = rows
  await next()
})

router.get('/instructor/find_by_id/:id', async (ctx, next) => {   
  let [rows] =  await db.query("SELECT * FROM instructors where id = ? ", ctx.params.id)
  ctx.body = rows
  await next()
})

router.get('/course/find_by_id/:id', async (ctx, next) => {   
  let [rows] =  await db.query("SELECT * FROM courses where id = ? ", ctx.params.id)
  ctx.body = rows
  await next()
})

router.get('/course/find_by_price/:price', async (ctx, next) => {   
  let [rows] =  await db.query("SELECT * FROM courses where price = ? ", ctx.params.price)
  ctx.body = rows
  await next()
})
 
app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)
