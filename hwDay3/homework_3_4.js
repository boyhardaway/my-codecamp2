'use strict'
let obj = { a: 7, b: 8 };
let ary = [1, 2, 3, 4];
let ary2 = [1, 2, { a: 1, b: 2 }];
let ary3 = [1, 2, { a: 1, b: { c: 3, d: 4 } }, [7, 8, 9]];

let newObj = cloneObj(ary3);
 
// ary3[2].a = 1123
ary3[2].b.c = 777
newObj[2].b.c = 567
console.log(ary3);
console.log(newObj);

function cloneObj(obj) {
    // console.log(Array.isArray(obj));
    let newObj;
    if (Array.isArray(obj)) {
        newObj = [];
        // for (let i in obj) {
        //     if (typeof (obj[i]) == 'object' || typeof (obj[i]) == 'array') {
        //         newObj[i] = cloneObj(obj[i]);
        //     } else {
        //         newObj.push(obj[i]);  //Same newObj[i] = obj[i];
        //     }
        // }
    } else {
        newObj = {};
        // for (let i in obj) {
        //     if (typeof (obj[i]) == 'object' || typeof (obj[i]) == 'array') {
        //         newObj[i] = cloneObj(obj[i]);
        //     } else {
        //         newObj[i] = obj[i]; 
        //     }
        // }
    }
    for (let i in obj) {
        if (typeof (obj[i]) == 'object' || typeof (obj[i]) == 'array') {
            newObj[i] = cloneObj(obj[i]);
        } else {
            newObj[i] = obj[i];
        }
    }
    return newObj;
} 