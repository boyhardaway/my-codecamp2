'use strict'
fetch('homework_3_1.json').then(response => {
    return response.json();
})
    .then(employees => {
        // debugger;
        let newEmp = addAdditionalFields(employees);
        newEmp[0].salary = "9999999";
        employees[1].id = "11111"
        genTable(employees);
        genTable(newEmp);      

    })
    .catch(error => {
        console.log('Error:', error);
    });

function addAdditionalFields(employees) {
    // ---> Create Arrey and push object
    let arryNewEmps = [];
    for (let j = 0; j < employees.length; j++) {
        let objNewEmp = {};
        let objEmp = employees[j];
        for (let i in objEmp) {
            objNewEmp[i] = objEmp[i];
        }
        arryNewEmps.push(objNewEmp);
    }

    for (let j = 0; j < arryNewEmps.length; j++) {
        addYearSalary(arryNewEmps[j]);
        addNextSalary(arryNewEmps[j]);
    }

    return arryNewEmps;
}

function addYearSalary(row) {
    row.yearSalary = row.salary * 12;
}

function addNextSalary(row) {
    let year = 3;
    row.nextSalary = [];
    let tempSalary = Math.round(row.salary);
    for (let i = 0; i < year; i++) {
        row.nextSalary.push(Math.round(tempSalary));
        tempSalary = tempSalary * 1.10;
    }
}


function genTable(employees) {
    $('#myTable').append($('<tr>'));
    for (let objSalaryHd in employees[0]) {
        $('#myTable').append($('<td>' + objSalaryHd + '</td>'));
    }
    $('#myTable').append($('</tr>'));
    for (let i = 0; i < employees.length; i++) {
        $('#myTable').append($('<tr>'));
        for (let objSalaryDt in employees[i]) {
            if (objSalaryDt === "nextSalary") {
                let tdd = '<td><ol>'
                for (let nextSly in employees[i].nextSalary) {
                    tdd += '<li>' + employees[i].nextSalary[nextSly] + '</li>'
                }
                $('#myTable').append($(tdd + '</ol></td>'));
            } else {
                $('#myTable').append($('<td>' + employees[i][objSalaryDt] + '</td>'));
            }
        }
        $('#myTable').append($('</tr>'));
    }
}


