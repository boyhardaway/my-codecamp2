'use strict'
fetch('homework_3_1.json').then(response => {
    return response.json();
})
    .then(employees => {
        addAdditionalFields(employees);
    })
    .catch(error => {
        console.error('Error:', error);
    });

function addAdditionalFields(employees){
    for (let i = 0; i < employees.length; i++) {
        addYearSalary(employees[i]);
        addNextSalary(employees[i]);
    }
    genTable(employees);
}

function addYearSalary(row) {
    row.yearSalary = row.salary * 12;
}

function addNextSalary(row) {
     let year = 3;
     row.nextSalary = []; 
     let tempSalary = Math.round(row.salary);
     for (let i = 0; i < year; i++) {
        row.nextSalary.push(Math.round(tempSalary));
        tempSalary = tempSalary * 1.10;
     }
}
 

function genTable(employees) {
    $('#myTable').append($('<tr>'));
    for (let objSalaryHd in employees[0]) { 
        $('#myTable').append($('<td>' + objSalaryHd + '</td>'));   
    }
    $('#myTable').append($('</tr>'));
    for (let i = 0; i < employees.length; i++) {
        $('#myTable').append($('<tr>'));
        for (let objSalaryDt in employees[i]) {
             if (objSalaryDt === "nextSalary") {
                 let tdd = '<td><ol>'
                 //$('#myTable').append($('<td>')); บางทีมัน Auto close td
                 //$('#myTable').append($('<ol>'));
                 for (let nextSly in employees[i].nextSalary) { 
                     tdd += '<li>' + employees[i].nextSalary[nextSly] + '</li>'
                 } 
                 $('#myTable').append($(tdd + '</ol></td>'));
             } else{
                 $('#myTable').append($('<td>' + employees[i][objSalaryDt] + '</td>'));
             }
        }
        $('#myTable').append($('</tr>'));
    }
}


