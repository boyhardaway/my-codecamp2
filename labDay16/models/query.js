

const db = require('../lib/db.js')

module.exports = {
    async myQuery(sql, val) {
        let [rows, fields] = await db.query(sql, val)
        console.log("The solution is: ", rows, fields)
        return rows
      }
}